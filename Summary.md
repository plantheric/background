[Background](https://bitbucket.org/plantheric/background) is an OS X application for setting the desktop background image.  
The built in image selector in the System Preferences is small and fiddly to use. Background allows easy selection and previewing of images, and  also includes additional sources for images.
