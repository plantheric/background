//
//  NewFlickrSource.h
//  Background
//
//  Created by Mark Bartlett on 15/02/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#import <Cocoa/Cocoa.h>
@class OFFlickrAPIContext;

@interface NewFlickrSource : NSWindowController

- (id)initWithFlickrContext:(OFFlickrAPIContext*)flickrContext;
- (IBAction)cancelButton:(id)sender;
- (IBAction)okButton:(id)sender;
- (IBAction)enteredUserName:(id)sender;
- (IBAction)sourceType:(id)sender;

@property (weak) IBOutlet NSTextField *enteredUserNameText;
@property (weak) IBOutlet NSTextField *realUserName;

@property NSArray* photoLists;

@property NSString* userId;
@property NSString* realName;

@property NSString* photoListId;
@property NSString* photoListName;

@property NSNumber* usePhotoSet;

@end
