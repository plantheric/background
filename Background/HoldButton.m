//
//  HoldButton.m
//  Background
//
//  Created by Mark Bartlett on 12/02/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#import "HoldButton.h"

@implementation HoldButton

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
	[super drawRect:dirtyRect];
	
    // Drawing code here.
}

- (void)mouseDown:(NSEvent*)event {
//	NSLog(@"Down");

	[self setState:NSOnState];
	[self sendAction:self.action to:self.target];
}

- (void)mouseUp:(NSEvent*)event {
//	NSLog(@"Up");

	[self setState:NSOffState];
	[self sendAction:self.action to:self.target];
}

- (void)mouseExited:(NSEvent*)event {
//	NSLog(@"Exit");

	[self setState:NSOffState];
	[self sendAction:self.action to:self.target];
}

@end
