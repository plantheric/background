//
//  BackgroundPreview.m
//  Background
//
//  Created by Mark Bartlett on 04/12/2012.
//  Copyright (c) 2012 plantheric. All rights reserved.
//

#import "BackgroundPreview.h"

@interface BackgroundPreview ()

@property NSURL *previousBackground;
@property NSDictionary *options;
@property NSDictionary *previousOptions;

@property NSWindow *window;
@property NSDate *timeShown;
@property NSMutableArray *applicationsToShow;

@end


@implementation BackgroundPreview

- (id) initWithOptions:(NSDictionary*)options fromWindow:(NSWindow *)window {
	
	self = [super init];
	
	if (self != nil) {
		self.options = options;
		self.window = window;

		NSWorkspace *workspace = [NSWorkspace sharedWorkspace];
		
		//	Current background
		self.previousBackground = [workspace desktopImageURLForScreen:self.window.screen];
		self.previousOptions = [workspace desktopImageOptionsForScreen:self.window.screen];
	}
	return self;
}

- (id) initWithUrl:(NSURL*)url withOptions:(NSDictionary*)options fromWindow:(NSWindow *)window {
	
	self = [self initWithOptions:options fromWindow:window];
	
	if (self != nil) {
		[self setURL:url];
	}
	return self;
}

- (void) setURL:(NSURL *)url {
	NSWorkspace *workspace = [NSWorkspace sharedWorkspace];
		
	[workspace setDesktopImageURL:url forScreen:self.window.screen options:self.options error:nil];
	
	//	Hide other applications
	self.applicationsToShow = [NSMutableArray array];
	NSArray *applications = [workspace runningApplications];
	
	for (NSRunningApplication *application in applications) {
		if (application.hidden == NO)
			[self.applicationsToShow addObject:application];
	}
	[workspace hideOtherApplications];
	
	
	[self.window setAlphaValue:0];
	
	self.timeShown = [NSDate date];

}

- (void) restoreBackground {
	
	//	Dont try to switch backgrounds too quickly
	double interval = -[self.timeShown timeIntervalSinceNow];
	if (interval < 0.5) {
		[NSThread sleepForTimeInterval:0.5];
	}

	NSWorkspace *workspace = [NSWorkspace sharedWorkspace];

	[workspace setDesktopImageURL:self.previousBackground forScreen:self.window.screen options:self.previousOptions error:nil];
	
	NSArray *applications = [workspace runningApplications];
	
	for (NSRunningApplication *application in applications) {
		if ([self.applicationsToShow containsObject:application])
			[application unhide];
	}

	[self.window setAlphaValue:1];
}


@end

