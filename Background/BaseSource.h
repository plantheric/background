//
//  BaseSource.h
//  Background
//
//  Created by Mark Bartlett on 12/02/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseSource : NSObject

@property NSString	*label;
@property NSArray	*children;
@property (readonly) BOOL	hasImages;

- (id)initWithLabel:(NSString*)label hasImages:(BOOL)hasImages;
- (NSArray*) cachedLocalFileURLs;

@end
