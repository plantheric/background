//
//  FlickrGroupSource.h
//  Background
//
//  Created by Mark Bartlett on 15/02/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#import "GroupSource.h"

@interface FlickrGroupSource : GroupSource

- (id)init;
- (ImageSource*)newImageSource;
-(void) addImageSource:(ImageSource*) folder;
-(void) removeImageSource:(ImageSource*) folder;

@end
