//
//  ImageSource.m
//  Background
//
//  Created by Mark Bartlett on 28/11/2012.
//  Copyright (c) 2012 plantheric. All rights reserved.
//

#import "FolderSource.h"
#import "ImageData.h"

@interface FolderSource()


@end

@implementation FolderSource

- (id)initWithURL:(NSURL *)url
{
	self = [super initWithLabel:[url lastPathComponent]];
	if (self) {
		_url = url;
		NSMutableArray *children =  [NSMutableArray array];

		NSArray *urls = [[NSFileManager defaultManager] contentsOfDirectoryAtURL: self.url
													  includingPropertiesForKeys: @[NSURLIsDirectoryKey, NSURLIsHiddenKey, NSURLIsPackageKey]
																		 options: NSDirectoryEnumerationSkipsHiddenFiles
																		   error: nil];
		for (NSURL *childURL in urls) {
			NSDictionary *attributes = [childURL resourceValuesForKeys:@[NSURLIsDirectoryKey, NSURLIsHiddenKey, NSURLIsPackageKey] error:nil];
			
			if ([attributes[NSURLIsDirectoryKey] boolValue] == YES &&
				[attributes[NSURLIsHiddenKey] boolValue] == NO &&
				[attributes[NSURLIsPackageKey] boolValue] == NO) {
				
				[children addObject: [[FolderSource alloc] initWithURL:childURL]];
			}
		}

		self.children = children;
	}
	return self;
}

- (id)initWithURL:(NSURL*)url andLabel:(NSString*)label {
	self = [self initWithURL:url];
	self.label = label;
	return self;
}

- (NSArray*)getImageDatasFromSource {
	NSMutableArray *urls = [NSMutableArray array];

	NSArray *allURLs = [[NSFileManager defaultManager] contentsOfDirectoryAtURL: self.url
													 includingPropertiesForKeys: @[NSURLIsDirectoryKey, NSURLIsHiddenKey, NSURLIsPackageKey, NSURLTypeIdentifierKey]
																		options: NSDirectoryEnumerationSkipsHiddenFiles
																		  error: nil];
	for (NSURL *childURL in allURLs) {
		NSDictionary *attributes = [childURL resourceValuesForKeys:@[NSURLIsDirectoryKey, NSURLIsHiddenKey, NSURLIsPackageKey, NSURLTypeIdentifierKey] error:nil];

		if ([attributes[NSURLIsDirectoryKey] boolValue] == NO &&
			[attributes[NSURLIsHiddenKey] boolValue] == NO &&
			[attributes[NSURLIsPackageKey] boolValue] == NO &&
			[[NSWorkspace sharedWorkspace] type:attributes[NSURLTypeIdentifierKey] conformsToType:@"public.image"]) {
			
			[urls addObject:[[ImageData alloc] initWithURL:childURL]];
		}
	}

	return urls;
}

@end
