//
//  WindowController.h
//  Background
//
//  Created by Mark Bartlett on 22/11/2012.
//  Copyright (c) 2012 plantheric. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class ImageBrowserViewController;

@interface WindowController : NSWindowController <NSOutlineViewDelegate, NSOutlineViewDataSource> {
}

@property (strong) NSArray *sourceItems;

@property (strong) ImageBrowserViewController *imageBrowserViewController;

@property (weak) IBOutlet NSOutlineView *outlineView;
@property (weak) IBOutlet NSView *contentView;
@property (weak) IBOutlet NSSlider *zoomSlider;
@property (weak) IBOutlet NSProgressIndicator *progressIndicator;
@property (weak) IBOutlet NSButton *favouritesButton;
@property (weak) IBOutlet NSPopUpButton *fillStyleMenu;

- (IBAction)addItem:(id)sender;
- (IBAction)removeItem:(id)sender;
- (IBAction)setBackground:(id)sender;
- (IBAction)zoomChanged:(id)sender;
- (IBAction)favouritedImage:(id)sender;
- (IBAction)changeFillStyle:(id)sender;
- (IBAction)previewBackground:(id)sender;

@end
