//
//  AppDelegate.m
//  Background
//
//  Created by Mark Bartlett on 20/11/2012.
//  Copyright (c) 2012 plantheric. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender {
	return YES;
}

@end
