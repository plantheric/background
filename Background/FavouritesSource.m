//
//  FavouritesSource.m
//  Background
//
//  Created by Mark Bartlett on 13/12/2012.
//  Copyright (c) 2012 plantheric. All rights reserved.
//

#import "FavouritesSource.h"
#import	"ImageData.h"

@implementation FavouritesSource

- (id)init {
	return [super initWithLabel:@"Favourites" andStorageKey:@"Favourites"];
}

- (NSArray*)newImageListByAddingImage:(ImageData*)newImage {
	NSMutableArray *imageList = [NSMutableArray arrayWithCapacity:[self.images count]+1];
	
	[imageList addObject:newImage];
	
	for (ImageData *image in self.images) {
		if ([image.imageURL isEqual:newImage.imageURL] == NO)
			[imageList addObject:image];
	}
	
	return imageList;
}

- (BOOL)isImageInFavourites:(ImageData*)image {
	
	return [self.images any:^(ImageData *i) { return [image.imageURL isEqual:i.imageURL]; }];
}


@end
