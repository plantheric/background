//
//  main.m
//  Background
//
//  Created by Mark Bartlett on 20/11/2012.
//  Copyright (c) 2012 plantheric. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
