//
//  ImageData.h
//  Background
//
//  Created by Mark Bartlett on 11/12/2012.
//  Copyright (c) 2012 plantheric. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageData : NSObject

@property NSURL *imageURL;
@property NSURL *thumbnailURL;
@property (readonly) NSURL *localFile;

- (id) initWithURL:(NSURL*)imageURL;
- (id) initWithImageURL:(NSURL*)imageURL andThumbnailURL:(NSURL*)thumbnailURL;

- (void) useLocalFileWithBlock:(void(^)(NSURL*))block;

@end
