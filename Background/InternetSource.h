//
//  InternetSource.h
//  Background
//
//  Created by Mark Bartlett on 06/12/2012.
//  Copyright (c) 2012 plantheric. All rights reserved.
//

#import "ImageSource.h"

@interface InternetSource : ImageSource

- (id) init;

@end
