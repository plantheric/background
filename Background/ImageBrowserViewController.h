//
//  ImageBrowserViewController.h
//  Background
//
//  Created by Mark Bartlett on 28/11/2012.
//  Copyright (c) 2012 plantheric. All rights reserved.
//

#import <Cocoa/Cocoa.h>
@class IKImageBrowserView;
@class ImageSource;
@class ImageData;

@interface ImageBrowserViewController : NSViewController

@property (weak) IBOutlet IKImageBrowserView *imageBrowser;
@property ImageSource *imageSource;

@property (strong) void (^selectionChanged)(ImageData*);

- (ImageData*)getImageDataForSelected;
- (void)updateItemsFromImageSource;

- (void)setImageZoom:(double)zoom;

@end
