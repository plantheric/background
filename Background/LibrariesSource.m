//
//  LibrariesSource.m
//  Background
//
//  Created by Mark Bartlett on 05/12/2012.
//  Copyright (c) 2012 plantheric. All rights reserved.
//

#import "LibrariesSource.h"
#import "iPhotoSource.h"

#import <MediaLibrary/MediaLibrary.h>

@interface LibrariesSource ()

@property NSDictionary* mediaSources;
@property MLMediaLibrary* library;


@end

@implementation LibrariesSource

static NSString *const MEDIASOURCESCONTEXT = @"MediaSources";

- (id) init {
	self = [super initWithLabel:@"Libraries"];
	
	if (self != nil) {

		self.children = @[];
		self.library = [[MLMediaLibrary alloc] initWithOptions:@{MLMediaLoadIncludeSourcesKey: @[MLMediaSourceiPhotoIdentifier]}];
		
		[self.library mediaSources];
		[self.library addObserver:self forKeyPath:@"mediaSources" options:0 context:(__bridge void *)(self)];
	}

	return self;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {

	if (context == (__bridge void *)(self)) {
		[self loadiPhotoLibrary];
	}
	else
		[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
}

- (void)loadiPhotoLibrary {

	MLMediaSource* iPhoto = self.library.mediaSources[@"com.apple.iPhoto"];
	
	if (iPhoto != nil) {
		self.children = [self.children arrayByAddingObject:[[iPhotoLibrarySource alloc] initWithMediaSource:iPhoto]];

		[[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshSourceList" object:nil];
	}
}
@end
