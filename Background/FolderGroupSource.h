//
//  FolderGroupSource.h
//  Background
//
//  Created by Mark Bartlett on 06/02/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#import "GroupSource.h"
@class ImageSource;

@interface FolderGroupSource : GroupSource

-(id) init;
-(ImageSource*) newImageSource;
-(void) addImageSource:(ImageSource*) folder;
-(void) removeImageSource:(ImageSource*) folder;

@end
