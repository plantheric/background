//
//  BaseSource.m
//  Background
//
//  Created by Mark Bartlett on 12/02/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#import "BaseSource.h"

@implementation BaseSource

- (id)initWithLabel:(NSString*)label hasImages:(BOOL)hasImages
{
    self = [super init];
    if (self) {
		self.label = label;
		_hasImages = hasImages;
    }
    return self;
}

- (NSArray*) cachedLocalFileURLs {
	NSMutableArray *cachedLocalFiles = [NSMutableArray array];
	
	for (BaseSource *source in self.children) {
		[cachedLocalFiles addObjectsFromArray:[source cachedLocalFileURLs]];
	}
	return cachedLocalFiles;
}
@end
