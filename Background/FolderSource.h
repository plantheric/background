//
//  ImageSource.h
//  Background
//
//  Created by Mark Bartlett on 28/11/2012.
//  Copyright (c) 2012 plantheric. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ImageSource.h"


@interface FolderSource : ImageSource

- (id)initWithURL:(NSURL*)url;
- (id)initWithURL:(NSURL*)url andLabel:(NSString*)label;

- (NSArray*)getImageDatasFromSource;

@property (readonly) NSURL *url;

@end
