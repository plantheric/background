//
//  iPhotoSource.m
//  Background
//
//  Created by Mark Bartlett on 05/12/2012.
//  Copyright (c) 2012 plantheric. All rights reserved.
//

#import "iPhotoSource.h"
#import "ImageData.h"
#import "NSArray+Extensions.h"

#import <MediaLibrary/MediaLibrary.h>

@interface iPhotoLibrarySource ()

@property MLMediaSource* mediaSource;

@end


@interface iPhotoAlbumListsSource : ImageSource

- (id)initWithLabel:(NSString *)label withMediaGroups:(NSArray*)mediaGroups;

@end


@interface iPhotoAlbumSource : ImageSource

- (id)initWithMediaGroup:(MLMediaGroup*)album;

@property MLMediaGroup *album;

@end


@implementation iPhotoLibrarySource

- (id)initWithMediaSource:(MLMediaSource*)source {

	self = [super initWithLabel:@"iPhoto"];
	if (self != nil) {

		self.mediaSource = source;
		[self.mediaSource rootMediaGroup];
		
		[source addObserver:self forKeyPath:@"rootMediaGroup" options:0 context:(__bridge void *)(self)];
	}
	return self;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {

	if (context == (__bridge void *)(self)) {

		MLMediaGroup* eventGroup = [self.mediaSource.rootMediaGroup.childGroups first:^(MLMediaGroup* group){
			return [group.typeIdentifier isEqualToString:MLiPhotoEventsFolderTypeIdentifier];
		}];
		
		NSArray* albumGroups = [self.mediaSource.rootMediaGroup.childGroups filter:^(MLMediaGroup* group){
			return [group.typeIdentifier isEqualToString:MLiPhotoAlbumTypeIdentifier];
		}];
		
		self.children = @[	[[iPhotoAlbumListsSource alloc] initWithLabel:@"Events" withMediaGroups:eventGroup.childGroups],
							[[iPhotoAlbumListsSource alloc] initWithLabel:@"Albums" withMediaGroups:albumGroups]
							];

		[[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshSourceList" object:nil];
	}
	else
		[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
}

@end


@implementation iPhotoAlbumListsSource

- (id)initWithLabel:(NSString *)label withMediaGroups:(NSArray*)mediaGroups {

	self = [super initWithLabel:label];
	if (self != nil) {
		
		self.children = [mediaGroups map:^(MLMediaGroup* album) {
			return [[iPhotoAlbumSource alloc] initWithMediaGroup:album];
		}];
	}
	return self;
}

@end


@implementation iPhotoAlbumSource

- (id)initWithMediaGroup:(MLMediaGroup*)album {

	NSString *label = album.attributes[@"name"];
	self = [super initWithLabel:label];

	if (self != nil) {
		self.album = album;
		
		[self.album addObserver:self forKeyPath:@"mediaObjects" options:0 context:(__bridge void *)(self)];
	}
	return self;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {

	if (context == (__bridge void *)(self)) {

		[[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshImageBrowser" object:nil];
	}
	else
		[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
}
	
- (NSArray*)getImageDatasFromSource {
	
	NSArray *urls = nil;
	
	if (self.album.mediaObjects != nil)
		urls = [self.album.mediaObjects map:^(MLMediaObject* photo) {
			return [[ImageData alloc] initWithURL:photo.URL];
		}];

	return urls;
}

@end
