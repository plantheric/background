//
//  AppDelegate.h
//  Background
//
//  Created by Mark Bartlett on 20/11/2012.
//  Copyright (c) 2012 plantheric. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@end
