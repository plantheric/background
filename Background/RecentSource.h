//
//  RecentSource.h
//  Background
//
//  Created by Mark Bartlett on 13/12/2012.
//  Copyright (c) 2012 plantheric. All rights reserved.
//

#import "PersistantCollectionSource.h"

@interface RecentSource : PersistantCollectionSource

- (id)init;

@end
