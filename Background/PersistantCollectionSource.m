//
//  PersistantCollectionSource.m
//  Background
//
//  Created by Mark Bartlett on 14/12/2012.
//  Copyright (c) 2012 plantheric. All rights reserved.
//

#import "PersistantCollectionSource.h"
#import "ImageData.h"

@interface PersistantCollectionSource ()

@property NSString *storageKey;

@end

@implementation PersistantCollectionSource

@synthesize images=_images;

- (id)initWithLabel:(NSString *)label andStorageKey:(NSString*)storageKey {

	self = [super initWithLabel:label];
	if (self != nil) {
		self.storageKey = storageKey;

		NSMutableArray *restored = [NSMutableArray array];
		
		NSArray *saved = [[NSUserDefaults standardUserDefaults] arrayForKey:self.storageKey];
		if (saved) {
			for (NSData *data in saved) {
				ImageData *image = [NSKeyedUnarchiver unarchiveObjectWithData:data];
				[restored addObject:image];
			}
		}
		_images = restored;
	}
	
	return self;
}


- (void)addImage:(ImageData *)imageToAdd {
	
	if (imageToAdd != nil) {
		_images = [self newImageListByAddingImage:imageToAdd];
		[self storeImageList];
	}
}

- (void)removeImage:(ImageData *)imageToRemove {
	
	_images = [self.images remove:^(ImageData *image) {
		return [image.imageURL isEqual:imageToRemove.imageURL];
	}];
	
	[self storeImageList];
}


- (void)storeImageList {
	
	NSArray *data = [self.images map:^(ImageData *image) { return [NSKeyedArchiver archivedDataWithRootObject:image]; }];

	[[NSUserDefaults standardUserDefaults] setObject:data forKey:self.storageKey];
}

- (NSArray*)newImageListByAddingImage:(ImageData*)newImage {
	return [NSArray array];
}

- (NSArray*) cachedLocalFileURLs {

	NSArray *localFileURLs = [[self.images remove:^BOOL(ImageData *image) {
		return image.localFile == nil || [image.localFile isEqual:image.imageURL];
	}] map:^(ImageData *image) {
		return image.localFile;
	}];

	return localFileURLs;
}

@end
