//
//  PreviewWindowController.m
//  Background
//
//  Created by Mark Bartlett on 03/12/2012.
//  Copyright (c) 2012 plantheric. All rights reserved.
//

#import <Quartz/Quartz.h>
#import "PreviewWindowController.h"

@interface PreviewWindowController ()

@property NSURL *url;

@end

@implementation PreviewWindowController

- (id) initWithWindowNibName:(NSString*)windowNibName url:(NSURL*)url
{
	self = [super initWithWindowNibName:windowNibName];
	if (self != nil) {
		self.url = url;
	}
	return self;
}



- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)awakeFromNib {

	[self.window setOpaque:NO];
	[self.window setBackgroundColor: [NSColor clearColor]];
	
}

- (void)windowDidLoad
{
    [super windowDidLoad];

	[self.window setFrame:self.window.screen.frame display:YES];

	[self.imageView setImageWithURL:self.url];
}

@end
