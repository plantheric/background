//
//  ImageData.m
//  Background
//
//  Created by Mark Bartlett on 11/12/2012.
//  Copyright (c) 2012 plantheric. All rights reserved.
//

#import "ImageData.h"

@interface ImageData () <NSURLDownloadDelegate, NSCoding>

@property (strong) void (^block)(NSURL*);

@property NSString *downloadFilePath;

@end

@implementation ImageData

- (id) initWithURL:(NSURL*)imageURL {
	return [self initWithImageURL:imageURL andThumbnailURL:imageURL];
}

- (id) initWithImageURL:(NSURL*)imageURL andThumbnailURL:(NSURL*)thumbnailURL {
	self = [super init];
	if (self != nil) {
		self.imageURL = imageURL;
		self.thumbnailURL = thumbnailURL;
		if ([self.imageURL isFileURL])
			_localFile = imageURL;
	}
	return self;
}

- (void) useLocalFileWithBlock:(void(^)(NSURL*))block {
	
	if (self.localFile != nil) {
		block (self.localFile);
	} else if (self.block == nil)
	{
		self.block = block;
		
		NSURL *docFolder = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:YES error:nil];
		
		self.downloadFilePath = [[docFolder URLByAppendingPathComponent:[self.imageURL lastPathComponent]] path];
		
		NSURLRequest *request = [[NSURLRequest alloc] initWithURL:self.imageURL];
		NSURLDownload *download = [[NSURLDownload alloc] initWithRequest:request delegate:self];
		
		if (download) {
			[download setDestination:self.downloadFilePath allowOverwrite:YES];
		}
	}
}

- (void) download:(NSURLDownload*)download didFailWithError:(NSError *)error {
	self.block = nil;
}

- (void) downloadDidFinish:(NSURLDownload *)download {

	_localFile = [NSURL fileURLWithPath:self.downloadFilePath];
	self.block (self.localFile);
	self.block = nil;
}

#pragma mark encode

- (id)initWithCoder:(NSCoder *)decoder {
	self = [super init];
	if (self != nil) {
		self.imageURL = [decoder decodeObjectForKey:@"imageURL"];
		self.thumbnailURL = [decoder decodeObjectForKey:@"thumbnailURL"];
		_localFile = [decoder decodeObjectForKey:@"localFile"];
		
		if ([[NSFileManager defaultManager] fileExistsAtPath:[self.localFile path]] == NO)
			_localFile = nil;
	}
	return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
	[coder encodeObject:self.imageURL forKey:@"imageURL"];
	[coder encodeObject:self.thumbnailURL forKey:@"thumbnailURL"];
	[coder encodeObject:self.localFile forKey:@"localFile"];
}

@end

