//
//  RecentSource.m
//  Background
//
//  Created by Mark Bartlett on 13/12/2012.
//  Copyright (c) 2012 plantheric. All rights reserved.
//

#import "RecentSource.h"
#import "ImageData.h"

@implementation RecentSource

const int maxRecentImages = 10;

- (id)init {
	return [super initWithLabel:@"Recent" andStorageKey:@"Recent"];
}

- (NSArray*)newImageListByAddingImage:(ImageData*)newImage {
	NSMutableArray *imageList = [NSMutableArray arrayWithCapacity:[self.images count]+1];

	[imageList addObject:newImage];
	
	for (ImageData *image in self.images) {
		if (imageList.count >= maxRecentImages)
			break;
			
		if ([image.imageURL isEqual:newImage.imageURL] == NO)
			[imageList addObject:image];
	}

	return imageList;
}

@end
