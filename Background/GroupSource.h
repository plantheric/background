//
//  GroupSource.h
//  Background
//
//  Created by Mark Bartlett on 12/02/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#import "BaseSource.h"

@class ImageSource;

@interface GroupSource : BaseSource

-(id) initWithLabel:(NSString*)label;

-(ImageSource*) newImageSource;
-(void) addImageSource:(ImageSource*) folder;
-(void) removeImageSource:(ImageSource*) folder;


@end
