//
//  FlickrSource.h
//  Background
//
//  Created by Mark Bartlett on 06/12/2012.
//  Copyright (c) 2012 plantheric. All rights reserved.
//

#import "ImageSource.h"
@class OFFlickrAPIContext;

@interface FlickrSource : ImageSource

- (id)initWithLabel:(NSString*)label apiContext:(OFFlickrAPIContext*)context api:(NSString*)url arguments:(NSDictionary*)arguements photosKey:(NSString*)keyPath;

@property (readonly) NSString* apiUrl;
@property (readonly) NSDictionary* arguments;
@property (readonly) NSString* photosKeyPath;

@end
