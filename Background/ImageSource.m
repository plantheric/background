//
//  ImageSource.m
//  Background
//
//  Created by Mark Bartlett on 28/11/2012.
//  Copyright (c) 2012 plantheric. All rights reserved.
//

#import "ImageSource.h"

@implementation ImageSource

@synthesize images=_images;

- (id)initWithLabel:(NSString*)label {

	self = [super initWithLabel:label hasImages:YES];
	return self;
}

- (NSArray*)getImageDatasFromSource {
	return nil;
}

- (NSArray*)images {

	if (_images == nil)	{
		_images = [self getImageDatasFromSource];
	}
	return _images;
}

- (void) setAsBuiltIn {
	_builtIn = TRUE;
}



@end

