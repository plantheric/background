//
//  FavouritesSource.h
//  Background
//
//  Created by Mark Bartlett on 13/12/2012.
//  Copyright (c) 2012 plantheric. All rights reserved.
//

#import "PersistantCollectionSource.h"
@class ImageData;

@interface FavouritesSource : PersistantCollectionSource

- (id)init;

- (BOOL)isImageInFavourites:(ImageData*)image;

@end
