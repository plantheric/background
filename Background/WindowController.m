//
//  WindowController.m
//  Background
//
//  Created by Mark Bartlett on 22/11/2012.
//  Copyright (c) 2012 plantheric. All rights reserved.
//

#import "WindowController.h"
#import "FolderGroupSource.h"
#import "FolderSource.h"
#import "LibrariesSource.h"
#import "InternetSource.h"
#import "RecentSource.h"
#import "FavouritesSource.h"
#import "ImageData.h"

#import "ImageBrowserViewController.h"
#import "PreviewWindowController.h"
#import "BackgroundPreview.h"

#import "QuartzCore/QuartzCore.h"

@interface WindowController ()

- (NSArray*)children:(id)item;
- (void)tidyDocumentsFolderWithExceptions:(NSArray*)exceptions;

@property BackgroundPreview	*backgroundPreview;
@property NSDictionary *desktopImageOptions;
@property NSArray *fillstyles;

@property id notificationObserver;

@end

@implementation WindowController


FolderGroupSource *folderSourceItems;
LibrariesSource *librariesSourceItems;
InternetSource *internetSourceItems;
ImageSource *otherSourceItems;

RecentSource *recentSourceItems;
FavouritesSource *favouriteSourceItems;

- (id)initWithWindow:(NSWindow *)window {
    self = [super initWithWindow:window];
    if (self) {
		
		self.fillstyles = @[@{NSWorkspaceDesktopImageScalingKey: @(NSImageScaleProportionallyUpOrDown), NSWorkspaceDesktopImageAllowClippingKey: @YES},
							@{NSWorkspaceDesktopImageScalingKey: @(NSImageScaleProportionallyUpOrDown), NSWorkspaceDesktopImageAllowClippingKey: @NO },
							@{NSWorkspaceDesktopImageScalingKey: @(NSImageScaleAxesIndependently),		NSWorkspaceDesktopImageAllowClippingKey: @NO },
							@{NSWorkspaceDesktopImageScalingKey: @(NSImageScaleNone),					NSWorkspaceDesktopImageAllowClippingKey: @NO }
						];

		folderSourceItems = [[FolderGroupSource alloc] init];
		librariesSourceItems = [[LibrariesSource alloc] init];
		internetSourceItems = [[InternetSource alloc] init];
		otherSourceItems = [[ImageSource alloc] initWithLabel:@"Other"];
		
		recentSourceItems = [[RecentSource alloc] init];
		favouriteSourceItems = [[FavouritesSource alloc] init];

		otherSourceItems.children = @[recentSourceItems, favouriteSourceItems];

		self.sourceItems = @[folderSourceItems, librariesSourceItems, internetSourceItems, otherSourceItems];
	}
    
    return self;
}

- (void)awakeFromNib {
	[self.outlineView expandItem:folderSourceItems];
	[self.outlineView expandItem:librariesSourceItems];
	[self.outlineView expandItem:internetSourceItems];
	[self.outlineView expandItem:otherSourceItems];
	
	self.imageBrowserViewController = [[ImageBrowserViewController alloc] init];
	
	id controller = self;
	self.imageBrowserViewController.selectionChanged = ^(ImageData *image) {
		[controller updateFavouriteButtonForImage:image];
	};
	
	NSView *subView = [self.imageBrowserViewController view];
	[subView setTranslatesAutoresizingMaskIntoConstraints:NO];
	
	[self.contentView addSubview:subView];

	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[subView]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(subView)]];
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[subView]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(subView)]];

	[[self.progressIndicator layer] setZPosition:1.0];
	
	//	Retrieve defaults
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

	long selectedRow =[defaults integerForKey:@"SourceSelection"];
	id selectedItem = [self.outlineView itemAtRow:selectedRow];

	if ([self outlineView:self.outlineView isGroupItem:selectedItem] == NO) {
		[self.outlineView selectRowIndexes:[NSIndexSet indexSetWithIndex:selectedRow] byExtendingSelection:NO];
	}

	double zoom = [defaults doubleForKey:@"ImageBrowserZoom"];
	[self.zoomSlider setDoubleValue:zoom];
	[self.imageBrowserViewController setImageZoom:zoom];

	self.desktopImageOptions = [[NSWorkspace sharedWorkspace] desktopImageOptionsForScreen:self.window.screen];

	self.notificationObserver = [[NSNotificationCenter defaultCenter] addObserverForName:@"RefreshSourceList"
																				  object:nil
																				   queue:[NSOperationQueue mainQueue]
																			  usingBlock:^(NSNotification *not) {
																				  [self.outlineView reloadData];
																			  }];
}

- (void)windowWillClose:(NSNotification*)notification {

	//	Store defaults
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

	[defaults setDouble:[self.zoomSlider doubleValue] forKey:@"ImageBrowserZoom"];
	[defaults setInteger:[self.outlineView selectedRow] forKey:@"SourceSelection"];

	
	[defaults synchronize];

	NSMutableArray *filesToKeep = [NSMutableArray array];

	for (ImageSource *source in self.sourceItems) {
		[filesToKeep addObjectsFromArray:[source cachedLocalFileURLs]];
	}

	[self tidyDocumentsFolderWithExceptions:filesToKeep];

	[[NSNotificationCenter defaultCenter] removeObserver:self.notificationObserver];
}

//	tidyDocumentsFolderWithExceptions
//
- (void)tidyDocumentsFolderWithExceptions:(NSArray*)filesToKeep {

	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSURL *docFolder = [fileManager URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];

	NSArray *urls = [fileManager contentsOfDirectoryAtURL: docFolder
							   includingPropertiesForKeys: @[NSURLTypeIdentifierKey]
												  options: NSDirectoryEnumerationSkipsHiddenFiles
													error: nil];
	for (NSURL *fileURL in urls) {
		NSDictionary *attributes = [fileURL resourceValuesForKeys:@[NSURLTypeIdentifierKey] error:nil];
		
		if ([filesToKeep containsObject:fileURL] == NO &&
			[[NSWorkspace sharedWorkspace] type:attributes[NSURLTypeIdentifierKey] conformsToType:@"public.image"]) {
			[fileManager removeItemAtURL:fileURL error:nil];
		}
	}

}

#pragma mark NSOutlineViewDelegate

- (BOOL)outlineView:(NSOutlineView *)outlineView isGroupItem:(id)item {
	return [outlineView parentForItem:item] == nil ? YES : NO;
}

- (BOOL)outlineView:(NSOutlineView *)outlineView shouldShowOutlineCellForItem:(id)item {
	return YES;
}

- (BOOL)outlineView:(NSOutlineView *)outlineView shouldSelectItem:(id)item {
	return [outlineView parentForItem:item] == nil ? NO : YES;
}

#pragma mark NSOutlineViewDataSource

- (NSArray*)children: (id)item {
	return item == nil ? self.sourceItems : ((ImageSource*)item).children;
}


- (NSInteger)outlineView:(NSOutlineView *)outlineView numberOfChildrenOfItem:(id)item {
	return [[self children:item] count];
}

- (BOOL)outlineView:(NSOutlineView *)outlineView isItemExpandable:(id)item {
	return ([[self children:item] count] > 0 || [self outlineView:outlineView isGroupItem:item]) ? YES : NO;
}

- (id)outlineView:(NSOutlineView *)outlineView child:(NSInteger)index ofItem:(id)item {
	return [[self children:item] objectAtIndex:index];
}

- (id)outlineView:(NSOutlineView *)outlineView objectValueForTableColumn:(NSTableColumn *)tableColumn byItem:(id)item {
	return [self outlineView:outlineView isGroupItem:item] ? [[item label] uppercaseString] : [item label];
}

- (void)outlineViewSelectionDidChange:(NSNotification *)notification {
	
	BaseSource *selectedSource = [self.outlineView itemAtRow:[self.outlineView selectedRow]];
	
	if (selectedSource.hasImages) {
		self.imageBrowserViewController.imageSource = (ImageSource*)selectedSource;
	}
}


- (GroupSource*)getGroupSourceForSource:(BaseSource*)source {

	while (source != nil  && source.hasImages == YES) {
		source = [self.outlineView parentForItem:source];
	}
	return (GroupSource*)source;
}

#pragma mark Actions

- (IBAction)addItem:(id)sender {
	BaseSource *selectedSource = [self.outlineView itemAtRow:[self.outlineView selectedRow]];
	GroupSource	*groupSource = [self getGroupSourceForSource:selectedSource];

	ImageSource* newSource = [groupSource newImageSource];

	if (newSource != nil) {
		[groupSource addImageSource:newSource];
		[[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshSourceList" object:nil];
	}
}

- (IBAction)removeItem:(id)sender {
	ImageSource *selectedSource = [self.outlineView itemAtRow:[self.outlineView selectedRow]];
	GroupSource	*groupSource = [self getGroupSourceForSource:selectedSource];

	[groupSource removeImageSource:selectedSource];

	[[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshSourceList" object:nil];
}

- (IBAction)setBackground:(id)sender {
	ImageData *image = [self.imageBrowserViewController getImageDataForSelected];

	[recentSourceItems addImage:image];
	if ([self.imageBrowserViewController.imageSource isEqual:recentSourceItems])
		[self.imageBrowserViewController updateItemsFromImageSource];

	[image useLocalFileWithBlock:^(NSURL *url){
		[[NSWorkspace sharedWorkspace] setDesktopImageURL:url forScreen:self.window.screen options:self.desktopImageOptions error:nil];
		}];
}

- (void)keyDown:(NSEvent *)event {
	NSString *characters = [event charactersIgnoringModifiers];
	
	if ([characters isEqualToString:@" "] && [event isARepeat] == NO) {
		[self showPreview:YES];
	}
}

- (void)keyUp:(NSEvent *)event {
	NSString *characters = [event charactersIgnoringModifiers];
	
	if ([characters isEqualToString:@" "]) {
		[self showPreview:NO];
	}
}

- (IBAction)zoomChanged:(id)sender {
	[self.imageBrowserViewController setImageZoom:[self.zoomSlider doubleValue]];
}

- (IBAction)favouritedImage:(id)sender {
	ImageData *image = [self.imageBrowserViewController getImageDataForSelected];

	if ([favouriteSourceItems isImageInFavourites:image])
		[favouriteSourceItems removeImage:image];
	else
		[favouriteSourceItems addImage:image];

	if ([self.imageBrowserViewController.imageSource isEqual:favouriteSourceItems])
		[self.imageBrowserViewController updateItemsFromImageSource];

	[self updateFavouriteButtonForImage:image];
}

- (IBAction)changeFillStyle:(id)sender {

	NSInteger selection = [self.fillStyleMenu indexOfSelectedItem];

	if (selection >= 0 && selection < [self.fillstyles count])
		self.desktopImageOptions = self.fillstyles[selection];
}


- (IBAction)previewBackground:(id)sender {
	
	BOOL showPreview = ([sender state] == NSOnState) ? YES : NO;

	[self showPreview:showPreview];
}

- (void)showPreview:(BOOL)show {
	
	if (show) {

		self.backgroundPreview = [[BackgroundPreview alloc] initWithOptions:self.desktopImageOptions fromWindow:self.window];
		ImageData *image = [self.imageBrowserViewController getImageDataForSelected];
		[self.progressIndicator startAnimation:self];
		//		CIFilter *blurFilter = [CIFilter filterWithName:@"CIGaussianBlur"];
		//		[blurFilter setDefaults];
		
		//		self.progressIndicator.layer.backgroundFilters = @[blurFilter];
		
		[image useLocalFileWithBlock:^(NSURL *url){
			[self.backgroundPreview setURL:url];
			self.progressIndicator.layer.backgroundFilters = @[];
			[self.progressIndicator stopAnimation:self];
			[self.window display];
		}];
	}
	else if (!show) {

		[self.backgroundPreview restoreBackground];
		self.backgroundPreview = nil;
		self.progressIndicator.layer.backgroundFilters = @[];
		[self.progressIndicator stopAnimation:self];
		[self.window display];
	}
	
}

- (void)updateFavouriteButtonForImage:(ImageData*)image {
	
	if ([favouriteSourceItems isImageInFavourites:image])
		[self.favouritesButton setState:NSOnState];
	else
		[self.favouritesButton setState:NSOffState];
}

@end
