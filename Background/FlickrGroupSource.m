//
//  FlickrGroupSource.m
//  Background
//
//  Created by Mark Bartlett on 15/02/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#import "FlickrGroupSource.h"
#import "FlickrSource.h"
#import "NewFlickrSource.h"

#import <ObjectiveFlickr/ObjectiveFlickr.h>
#include "FlickrKeyAndSecret.h"

static NSString* STORAGE_KEY = @"FlickrGroup";

@interface FlickrGroupSource ()

@property OFFlickrAPIContext* flickrContext;
@end

@implementation FlickrGroupSource

- (id)init {
	self = [super initWithLabel:@"Flickr"];
	
	if (self != nil) {
		
		self.flickrContext = [[OFFlickrAPIContext alloc] initWithAPIKey:FLICKR_KEY sharedSecret:FLICKR_SEC];
		NSDictionary *arguments = @{@"per_page": @"100", @"extras": @"license"};
		
		self.children = @[
						  [[FlickrSource alloc] initWithLabel:@"Interestingness" apiContext:self.flickrContext api:@"flickr.interestingness.getList" arguments:arguments photosKey:@"photos.photo"],
						  [[FlickrSource alloc] initWithLabel:@"Recent" apiContext:self.flickrContext api:@"flickr.photos.getRecent" arguments:arguments photosKey:@"photos.photo"]
						  ];

		[self.children each:^(FlickrSource *source) { [source setAsBuiltIn]; }];

		NSArray *saved = [[NSUserDefaults standardUserDefaults] arrayForKey:STORAGE_KEY];
		if (saved) {
			NSMutableArray *restored =  [NSMutableArray array];
			
			for (NSDictionary *data in saved) {

				[restored addObject: [[FlickrSource alloc] initWithLabel:data[@"label"]
															  apiContext:self.flickrContext
																	 api:data[@"apiUrl"]
															   arguments:data[@"arguments"]
															   photosKey:data[@"photosKeyPath"]]];
			}
			self.children = [self.children arrayByAddingObjectsFromArray:restored];
		}
	}

	return self;
}

- (ImageSource*)newImageSource {

	NewFlickrSource *controller = [[NewFlickrSource alloc] initWithFlickrContext:self.flickrContext];
	long response = [NSApp runModalForWindow:controller.window];

	FlickrSource* source = nil;
	if (response == NSModalResponseOK && controller.userId != nil) {

		if (controller.usePhotoSet.intValue == 2) {
			source = [[FlickrSource alloc] initWithLabel:controller.photoListName
											  apiContext:self.flickrContext
													 api:@"flickr.galleries.getPhotos"
											   arguments:@{@"per_page": @"50", @"gallery_id": controller.photoListId}
											   photosKey:@"photos.photo"];
		} else if (controller.usePhotoSet.intValue == 1) {
			source = [[FlickrSource alloc] initWithLabel:controller.photoListName
											  apiContext:self.flickrContext
													 api:@"flickr.photosets.getPhotos"
											   arguments:@{@"per_page": @"50", @"photoset_id": controller.photoListId}
											   photosKey:@"photoset.photo"];
		} else {
			source = [[FlickrSource alloc] initWithLabel:@"Photostream"
											  apiContext:self.flickrContext
													 api:@"flickr.people.getPublicPhotos"
											  arguments:@{@"per_page": @"50", @"user_id": controller.userId}
											   photosKey:@"photos.photo"];
		}
	}
	return source;
}

-(void) addImageSource:(FlickrSource*) folder {
	
	[super addImageSource:folder];
	[self storeSourceList];
}

-(void) removeImageSource:(FlickrSource*) folder {
	
	[super removeImageSource:folder];
	[self storeSourceList];
}


- (void)storeSourceList {
	
	NSArray* sourcesToStore = [self.children remove:^(FlickrSource* source) { return source.builtIn; }];
	
	NSArray* data = [sourcesToStore map:^(FlickrSource* source){return [source dictionaryWithValuesForKeys:@[@"label", @"apiUrl", @"arguments", @"photosKeyPath"]];}];

	[[NSUserDefaults standardUserDefaults] setObject:data forKey:STORAGE_KEY];
}

@end
