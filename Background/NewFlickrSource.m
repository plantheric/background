//
//  NewFlickrSource.m
//  Background
//
//  Created by Mark Bartlett on 15/02/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#import "NewFlickrSource.h"
#import <ObjectiveFlickr/ObjectiveFlickr.h>

#import "NSArray+Extensions.h"

@interface Session : NSObject

@property NSString* responsePath;
@property (strong) void (^success)(id);
@property (strong) void (^failure)(NSError*);

@end

@interface NewFlickrSource () <OFFlickrAPIRequestDelegate>

- (void)sendRequestFor:(NSString*)url withArgument:(NSDictionary*)arguments getResponse:(NSString*)path successBlock:(void(^)(id))success failureBlock:(void(^)(NSError*))failure;

@property OFFlickrAPIContext* flickrContext;
@property NSMutableArray* requests;

@property NSArray* photoSets;
@property NSArray* galleries;

@end



@implementation Session

@end

@implementation NewFlickrSource

- (id)initWithFlickrContext:(OFFlickrAPIContext*)flickrContext
{
	self = [super initWithWindowNibName:@"NewFlickrSource"];
	if (self != nil) {
		self.flickrContext = flickrContext;
		self.photoSets = @[];
		self.photoListId = @"";
		self.usePhotoSet = @0;
	}
	return self;
}

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)awakeFromNib {

}

- (void)windowDidLoad
{
	self.requests = [NSMutableArray array];
    [super windowDidLoad];
}

- (void)windowWillClose:(NSNotification*)notification {
	[NSApp stopModalWithCode:NSModalResponseCancel];
}

- (IBAction)cancelButton:(id)sender {
	[NSApp stopModalWithCode:NSModalResponseCancel];
}

- (IBAction)okButton:(id)sender {
	
	self.photoListName = [self.photoLists first:^(id set){return [self.photoListId isEqualToString: set[@"id"]];}][@"title"][@"_text"];
	
	[NSApp stopModalWithCode:NSModalResponseOK];
}


- (IBAction)enteredUserName:(id)sender {

	[self getFlickrUserId];
}

- (void)getFlickrUserId {
	
	[self sendRequestFor: @"flickr.people.findByUsername"
			withArgument: @{@"username": self.enteredUserNameText.stringValue}
			 getResponse:  @"user"
			successBlock: ^(NSDictionary* resp) {
					
				self.userId = resp[@"nsid"];
				
				[self getFlickrRealName];
				[self getFlickrPhotosets];
				[self getFlickrGalleries];
			}
			failureBlock: ^(NSError* error) {

				self.userId = nil;
				self.realName = @"";
				self.photoSets = @[];
			}];
}

- (void)getFlickrRealName {
	
	[self sendRequestFor: @"flickr.people.getInfo"
			withArgument: @{@"user_id": self.userId}
			 getResponse:  @"person.realname"
			successBlock: ^(id resp) {
					
				self.realName = resp[@"_text"];
			}
			failureBlock: ^(NSError* error) {
				
			}];
}

- (void)getFlickrPhotosets {
	
	[self sendRequestFor: @"flickr.photosets.getList"
			withArgument: @{@"user_id": self.userId}
			 getResponse:  @"photosets.photoset"
			successBlock: ^(id resp) {
				
				resp = resp ? resp : @[];
				for (NSDictionary *photoset in resp) {
					NSLog(@"%@ - %@", photoset[@"title"][@"_text"], photoset[@"id"]);
				}
				self.photoSets = resp;
				self.photoListId = self.photoSets.count ? resp[0][@"id"] : nil;
				self.photoLists = self.photoSets;
			}
			failureBlock: ^(NSError* error) {
				self.photoSets = @[];
			}];
}

- (void)getFlickrGalleries {
	
	[self sendRequestFor: @"flickr.galleries.getList"
			withArgument: @{@"user_id": self.userId}
			 getResponse:  @"galleries.gallery"
			successBlock: ^(id resp) {
				
				resp = resp ? resp : @[];
				for (NSDictionary *gallery in resp) {
					NSLog(@"%@ - %@", gallery[@"title"][@"_text"], gallery[@"id"]);
				}
				self.galleries = resp;
			}
			failureBlock: ^(NSError* error) {
				self.galleries = @[];
			}];
}

- (IBAction)sourceType:(id)sender {
	NSMatrix* matrix = sender;
	
	if (matrix.selectedTag == 1) {
		self.photoLists = self.photoSets;
	} else if (matrix.selectedTag == 2) {
		self.photoLists = self.galleries;
	}

	self.photoListId = self.photoLists.count ? self.photoLists[0][@"id"] : nil;
}


- (void)sendRequestFor:(NSString*)url withArgument:(NSDictionary*)arguments getResponse:(NSString*)path successBlock:(void(^)(id))success failureBlock:(void(^)(NSError*))failure {
		
	OFFlickrAPIRequest* request = [[OFFlickrAPIRequest alloc] initWithAPIContext:self.flickrContext];
	request.delegate = self;
	
	Session* session  = [[Session alloc] init];
	session.responsePath = path;
	session.success = success;
	session.failure = failure;
	request.sessionInfo = session;
	
	[request callAPIMethodWithGET:url arguments:arguments];

	[self.requests addObject:request];
}

- (void)flickrAPIRequest:(OFFlickrAPIRequest *)inRequest didCompleteWithResponse:(NSDictionary *)inResponseDictionary {

	[self.requests removeObject:inRequest];

	Session* session = inRequest.sessionInfo;
	
	NSDictionary* resp = [inResponseDictionary valueForKeyPath:session.responsePath];
	session.success(resp);

}

- (void)flickrAPIRequest:(OFFlickrAPIRequest *)inRequest didFailWithError:(NSError *)inError {

	[self.requests removeObject:inRequest];

	Session* session = inRequest.sessionInfo;
	session.failure(inError);
}


@end
