//
//  PersistantCollectionSource.h
//  Background
//
//  Created by Mark Bartlett on 14/12/2012.
//  Copyright (c) 2012 plantheric. All rights reserved.
//

#import "ImageSource.h"
@class ImageData;

@interface PersistantCollectionSource : ImageSource

- (id)initWithLabel:(NSString *)label andStorageKey:(NSString*)storageKey;

- (void)addImage:(ImageData *)imageToAdd;
- (void)removeImage:(ImageData*)imageToRemove;


@end
