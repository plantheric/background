//
//  BackgroundPreview.h
//  Background
//
//  Created by Mark Bartlett on 04/12/2012.
//  Copyright (c) 2012 plantheric. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BackgroundPreview : NSObject

- (id) initWithUrl:(NSURL*)url withOptions:(NSDictionary*)options fromWindow:(NSWindow*)window;
- (id) initWithOptions:(NSDictionary*)options fromWindow:(NSWindow*)window;

- (void) setURL:(NSURL*)url;
- (void) restoreBackground;

@end
