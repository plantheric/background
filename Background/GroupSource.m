//
//  GroupSource.m
//  Background
//
//  Created by Mark Bartlett on 12/02/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#import "GroupSource.h"
#import "ImageSource.h"

@implementation GroupSource

-(id)initWithLabel:(NSString*)label {
	
	self = [super initWithLabel:label hasImages:NO];
	return self;
}

-(ImageSource*) newImageSource	{
	return nil;
}

-(void) addImageSource:(ImageSource*) folder {
	
	self.children = [self.children arrayByAddingObject:folder];	
}

-(void) removeImageSource:(ImageSource*) folder {
	
	self.children = [self.children remove:^(ImageSource* source ) {
		if (source == folder && source.builtIn == NO)
			return YES;
		else
			return NO;
	}];
}


@end
