//
//  FlickrSource.m
//  Background
//
//  Created by Mark Bartlett on 06/12/2012.
//  Copyright (c) 2012 plantheric. All rights reserved.
//

#import "FlickrSource.h"
#import "ImageData.h"
#import <ObjectiveFlickr/ObjectiveFlickr.h>


@interface FlickrSource () <OFFlickrAPIRequestDelegate>

@property OFFlickrAPIContext *apiContext;
@property OFFlickrAPIRequest *request;
@property NSArray *photos;

@end


@implementation FlickrSource

- (id)initWithLabel:(NSString*)label apiContext:(OFFlickrAPIContext*)context api:(NSString*)url
		 arguments:(NSDictionary*)arguements photosKey:(NSString*)keyPath {

	self = [super initWithLabel:label];
	if (self != nil) {
		_apiContext = context;
		_apiUrl = url;
		_arguments = arguements;
		_photosKeyPath = keyPath;
	}
	return self;	
}

- (NSArray*)getImageDatasFromSource {

	if (self.request == nil) {
		self.request = [[OFFlickrAPIRequest alloc] initWithAPIContext:self.apiContext];
		[self.request setDelegate:self];
		
		[self.request callAPIMethodWithGET:self.apiUrl arguments:self.arguments];
	}

	NSMutableArray *urls = nil;

	if (self.photos != nil) {

		urls = [NSMutableArray array];
		for (NSDictionary *photo in self.photos) {
			
			NSURL *imageURL = [self.apiContext photoSourceURLFromDictionary:photo size:OFFlickrLargeSize];
			NSURL *thumbURL = [self.apiContext photoSourceURLFromDictionary:photo size:OFFlickrSmallSize];
			[urls addObject:[[ImageData alloc] initWithImageURL:imageURL andThumbnailURL:thumbURL]];
		}
	}
	return urls;
}

- (void)flickrAPIRequest:(OFFlickrAPIRequest *)inRequest didCompleteWithResponse:(NSDictionary *)inResponseDictionary {
	NSArray *allPhotos = [inResponseDictionary valueForKeyPath:self.photosKeyPath];

	NSMutableArray *newPhotos = [NSMutableArray array];

	for (NSDictionary *photo in allPhotos) {
		
		NSString *license = photo[@"license"];
		
		if (![license isEqualToString:@"0"])
			[newPhotos addObject:photo];
	}

	self.photos = newPhotos;

	[[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshImageBrowser" object:nil];
}

- (void)flickrAPIRequest:(OFFlickrAPIRequest *)inRequest didFailWithError:(NSError *)inError {
	

}


@end
