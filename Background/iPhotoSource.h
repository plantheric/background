//
//  iPhotoSource.h
//  Background
//
//  Created by Mark Bartlett on 05/12/2012.
//  Copyright (c) 2012 plantheric. All rights reserved.
//

#import "ImageSource.h"
@class MLMediaSource;


@interface iPhotoLibrarySource : ImageSource

- (id)initWithMediaSource:(MLMediaSource*)source;

@end


