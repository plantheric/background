
//  ImageBrowserViewController.m
//  Background
//
//  Created by Mark Bartlett on 28/11/2012.
//  Copyright (c) 2012 plantheric. All rights reserved.
//

#import <Quartz/Quartz.h>
#import "ImageBrowserViewController.h"
#import	"ImageData.h"
#import "ImageSource.h"

@interface ImageItemObject : NSObject

@property ImageData *imageData;
@end

@implementation ImageItemObject

- (id)initWithImageData:(ImageData*) imageData {
	self = [super init];
	if (self != nil)
		self.imageData = imageData;
	return self;
}

- (NSString *)imageRepresentationType {
    return IKImageBrowserNSURLRepresentationType;
}

- (id)imageRepresentation {
	bool useLocaFile = 	 (self.imageData.localFile != nil) &&
						 [self.imageData.localFile isFileURL] &&
						([self.imageData.thumbnailURL isFileURL]== NO);

    return useLocaFile ? self.imageData.localFile : self.imageData.thumbnailURL;
}

- (NSString *) imageUID {
    return [self.imageData.imageURL absoluteString];
}

@end



@interface ImageBrowserViewController ()

@property NSArray *imageItems;
@property id notificationObserver;

@end

@implementation ImageBrowserViewController

@synthesize imageSource=_imageSource;

- (id)init {
    return [super initWithNibName:@"ImageBrowser" bundle:nil];
}

- (void)awakeFromNib {
    
	[self.imageBrowser setCellsStyleMask:IKCellsStyleShadowed];
	
	self.notificationObserver = [[NSNotificationCenter defaultCenter] addObserverForName:@"RefreshImageBrowser"
																			 object:nil
																			  queue:[NSOperationQueue mainQueue]
																		 usingBlock:^(NSNotification *not) {
																			  [self updateItemsFromImageSource];
																		 }];
}

- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self.notificationObserver];
}

- (void)setImageSource:(ImageSource *)imageSource {
	
	_imageSource = imageSource;

	[self updateItemsFromImageSource];
}

- (ImageSource*)imageSource {
	return _imageSource;
}

- (void)updateItemsFromImageSource {

	self.imageItems = [_imageSource.images map:^(ImageData *image) {
		return [[ImageItemObject alloc] initWithImageData:image];
	}];
	
	[self.imageBrowser reloadData];
}

- (ImageData*)getImageDataForSelected {
	NSIndexSet *indexes = [self.imageBrowser selectionIndexes];

	ImageData *image;
	if ([indexes count] > 0) {
		image = [self.imageSource.images objectAtIndex:[indexes firstIndex]];
	}
	return image;
}

- (void)setImageZoom:(double)zoom {
	[self.imageBrowser setZoomValue:zoom];
}


#pragma mark	 IKImageBrowserDataSource

- (NSUInteger)numberOfItemsInImageBrowser:(IKImageBrowserView*)browser {
	return [self.imageItems count];
}

- (id)imageBrowser:(IKImageBrowserView*)browser itemAtIndex:(NSUInteger)index {
	return [self.imageItems objectAtIndex:index];
}

#pragma mark	 IKImageBrowserDelegate

- (void)imageBrowser:(IKImageBrowserView*)browser cellWasDoubleClickedAtIndex:(NSUInteger)index {
	
}

- (void)imageBrowserSelectionDidChange:(IKImageBrowserView *)browser {
	self.selectionChanged([self getImageDataForSelected]);
}

@end
