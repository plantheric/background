//
//  LibrariesSource.h
//  Background
//
//  Created by Mark Bartlett on 05/12/2012.
//  Copyright (c) 2012 plantheric. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ImageSource.h"

@interface LibrariesSource : ImageSource

- (id) init;

@end
