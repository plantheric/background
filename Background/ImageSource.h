//
//  ImageSource.h
//  Background
//
//  Created by Mark Bartlett on 28/11/2012.
//  Copyright (c) 2012 plantheric. All rights reserved.
//

#import "BaseSource.h"

@interface ImageSource : BaseSource

@property (readonly) NSArray *images;
@property (readonly) BOOL builtIn;

- (id)initWithLabel:(NSString*)label;
- (void)setAsBuiltIn;

@end
