//
//  PreviewWindowController.h
//  Background
//
//  Created by Mark Bartlett on 03/12/2012.
//  Copyright (c) 2012 plantheric. All rights reserved.
//

#import <Cocoa/Cocoa.h>
@class IKImageView;

@interface PreviewWindowController : NSWindowController

- (id) initWithWindowNibName:(NSString*)windowNibName url:(NSURL*)url;

@property (weak) IBOutlet IKImageView *imageView;

@end
