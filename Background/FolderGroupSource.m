//
//  FolderGroupSource.m
//  Background
//
//  Created by Mark Bartlett on 06/02/2014.
//  Copyright (c) 2014 plantheric. All rights reserved.
//

#import "FolderGroupSource.h"
#import "FolderSource.h"

@implementation FolderGroupSource

NSString* STORAGE_KEY = @"FolderGroup";

-(id)init {
	self = [super initWithLabel:@"Folders"];
	
	if (self != nil) {
	
		NSURL *picsFolder = [[NSFileManager defaultManager] URLForDirectory:NSPicturesDirectory inDomain:NSUserDomainMask appropriateForURL:nil
																	 					create:YES error:nil];
		self.children = @[
					   [[FolderSource alloc] initWithURL:[picsFolder URLByResolvingSymlinksInPath]],
					   [[FolderSource alloc] initWithURL:[NSURL fileURLWithPath:@"/Library/Desktop Pictures"] andLabel:@"Apple Desktop" ]];

		[self.children each:^(FolderSource *source) {
			[source setAsBuiltIn];
		}];
			
		NSArray *saved = [[NSUserDefaults standardUserDefaults] arrayForKey:STORAGE_KEY];
		if (saved) {
			NSMutableArray *restored =  [NSMutableArray array];

			for (NSData *data in saved) {
				BOOL isStale;
				NSURL *url =  [NSURL URLByResolvingBookmarkData:data
													   options:NSURLBookmarkResolutionWithSecurityScope
												 relativeToURL:nil
										   bookmarkDataIsStale:&isStale
														 error:nil];

				if ([url startAccessingSecurityScopedResource] == YES) {
					[restored addObject: [[FolderSource alloc] initWithURL:url]];
				}
			}
			self.children = [self.children arrayByAddingObjectsFromArray:restored];
		}
	}
	return self;
}

-(ImageSource*) newImageSource	{

	NSOpenPanel *panel = [NSOpenPanel openPanel];
	[panel setCanChooseDirectories:YES];
	[panel setCanChooseFiles:NO];
	
	return ([panel runModal] == NSFileHandlingPanelOKButton) ? [[FolderSource alloc] initWithURL:panel.URL] : nil;
}

-(void) addImageSource:(FolderSource*) folder {
	
	[super addImageSource:folder];
	[self storeFolderList];
}

-(void) removeImageSource:(FolderSource*) folder {

	[super removeImageSource:folder];
	[self storeFolderList];
}


- (void)storeFolderList {

	NSArray *foldersToStore = [self.children remove:^(FolderSource* folder) { return folder.builtIn; }];

	NSArray *data = [foldersToStore map:^(FolderSource* folder) {
		
		return [folder.url bookmarkDataWithOptions:NSURLBookmarkCreationWithSecurityScope | NSURLBookmarkCreationSecurityScopeAllowOnlyReadAccess
					includingResourceValuesForKeys:@[]
									 relativeToURL:nil
											 error:nil];
		}];
	
	[[NSUserDefaults standardUserDefaults] setObject:data forKey:STORAGE_KEY];
}

@end
