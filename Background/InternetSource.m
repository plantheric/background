//
//  InternetSource.m
//  Background
//
//  Created by Mark Bartlett on 06/12/2012.
//  Copyright (c) 2012 plantheric. All rights reserved.
//

#import "InternetSource.h"
#import "FlickrGroupSource.h"

@implementation InternetSource

- (id) init {
	self = [super initWithLabel:@"Internet"];
	
	if (self != nil) {
		self.children = @[[[FlickrGroupSource alloc] init]];
	}
	
	return self;
}

@end
