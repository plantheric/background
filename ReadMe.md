# Background

Background is an OS X application for setting the desktop background image.

The built in image selector in the System Preferences is small and fiddly to use. In addition it has a limited number of source for images.
