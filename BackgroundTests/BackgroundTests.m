//
//  BackgroundTests.m
//  BackgroundTests
//
//  Created by Mark Bartlett on 20/11/2012.
//  Copyright (c) 2012 plantheric. All rights reserved.
//

#import "BackgroundTests.h"

@implementation BackgroundTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"Unit tests are not implemented yet in BackgroundTests");
}

@end
